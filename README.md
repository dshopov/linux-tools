# Linux Tools

To use those tools you need to put them in your $HOME/bin directory.

Add this lane to your ~/.bash_profile
export PATH=$PATH:/proj/inbox/ft/DMP/PDE/bin/:$HOME/bin

Tools.
ci - Create inbox

ew - Extract WDP

td - Test Downloader

Usage:

ci <INBOX_NAME>

ew </path/to/WDP/archive.zip> <INBOX_NAME>

td <WDP_ID> <INBOX_NAME> (Runs: TestWebDownloader.pl -p /proj/reception/<INBOX_NAME>/wdp/bin/<WDP_ID>.pl -c /proj/reception/<INBOX_NAME>/wdp/<WDP_ID>/<WDP_ID>_CONFIG.txt)